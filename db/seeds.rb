# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.create!(name:  "Example User",
             company: "Example Company",
             email: "example@test.org",
             password:              "passpass",
             password_confirmation: "passpass",
             admin: true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  company= "Company ex#{n+1}"
  email = "example-#{n+1}@test.org"
  password = "password"
  User.create!(name:  name,
               company:company,
               email: email,
               password:              password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
  title =Faker::Lorem.words(2..5)
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.posts.create!(title: title, content: content) }
end